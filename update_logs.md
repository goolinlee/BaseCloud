# BaseCloud - Release Notes
======================================
##1.6.0.20200905
优化layout组件，H5端显示退出和设置菜单，增加innerScroll属性。

##1.5.9.20200905
优化菜单模块，编辑菜单时增加级联菜单选择，增加菜单展示平台选项。优化conditions组件、labels组件、layout组件；更新db_init.json数据库初始化文件。

##1.5.8.20200904
操作日志改为前端转换日期，日期组件增加返回类型属性，可指定为timestamp类型。

##1.5.7.20200903
修复范围查询分页只指定数据库字段时查询报错的bug；增加multiply方法，用于两数相乘。

##1.5.6.20200903
管理后台界面适配移动端

##1.5.5.20200902
修复URL化POST请求接收纯字符串类参数时被处理为json的bug

##1.5.3.20200831
新增results和price组件

##1.5.2.20200831
增加内置图标;修复返回undefined时未处理默认响应结果的bug；

##1.5.1.20200830
修复URL化后，POST请求接收XML数据错误的问题。

##1.5.0.20200829
修复客户端缓存中对象值被更改后缓存错乱的bug。

##1.4.9.20200829
[优化]URL化后。POST请求下自动转换queryString类型的body为参数；[优化]业务函数返回除undefined、true、false外的非json类型的响应结果时，不做默认响应处理。

##1.4.8.20200828
增加内置图标；修复URL化后，POST请求下接收非json类参数报错的bug；

##1.4.7.20200827
增加getDeepValue方法，深度获取json值；增加deepMerge方法，深度合并json对象;修复isState方法的bug

##1.4.6.20200827
增加this.setConfigs()方法，方便每个云函数单独定义配置项。

##1.4.5.20200827
增加用于URL化的基础云函数curl，增加路由开放与禁止外部访问方案示例；拦截器支持默认响应结果；更新uni-id版本；

路由禁止使用相对路径访问业务函数目录外的其他目录；增加内置图标；组件兼容非H5端（暂未适配）。
访问路由中禁止使用相对路径访问，防止访问其他业务函数根目录以外的目录，增强云函数的安全性。

## 1.4.3.20200826
全局开启默认响应结果后，可在业务函数中临时关闭默认响应结果；业务函数返回state状态描述时，不再进行默认响应处理。使用this.forward()转发请求时，也进行默认响应结果处理。
新增isState()、isOk()、isFail()方法，用于判定响应结果。
更改云函数URL化后指定访问路由的方式，详见文档：[云函数URL化](http://localhost:8081/#/pages/functions/url)

默认响应结果，增加指定dataKey配置项，可自定义默认返回数据结构。

上传组件中的删除文件回调增加index和file数据。

完善内置图标库，使用文档：

## 如果你想入手云开发，本框架是绝佳的学习素材和项目快速搭建方案，加群交流：649807152
新增this.forward()方法，可直接在云函数内，转发请求到不同的路由下；优化内置属性this.configs，将内部属性设置为只读，避免配置项数据随意篡改而引起的混乱。

新增uploads组件，支持图片、音频、视频直传云存储；新增files组件，预览音频、视频、图片文件。增加图片文件加载中、加载失败状态显示。

## 1.2.8.20200822
增强upload-images组件，支持json数组类型的value，@change回调增加图片文件详细信息数据。
## 1.2.6.20200820
修复拦截器传递参数的bug；修复自定义路由模式下未自定义路由报错的bug；优化selects、multi-selects、layout、conditons、inputs等组件的样式。
## 1.2.0.20200819
1. 新增云函数url访问支持；
2. 新增访问路径重命名配置；
3. 优化菜单模块功能；
4. 优化layout组件。

## 1.1.9.20200818

优化弹窗组件、layout组件、auth-btn组件；优化菜单管理模块，新增编辑菜单改为弹窗模式，增加菜单折叠展开功能。

## 1.1.8.20200817
paginate()方法增加collectionName参数，可以只指定集合名称。
this.getPage()方法增加dataInDb可选参数，list和dataInDb二选一传入。
更新mores组件，增加auto属性，可以控制是否默认展开。
新增alerts弹窗组件

## 1.1.6.20200817
1. setMaxOrderNum方法增加步长参数，默认为10，自动生成排序序号后，方便插入新的序号排序。如果需要自增排序，将步长设置为1即可。
2. 修复表单重置后radios组件无法赋值的问题。

## 1.1.5.20200815
	* 【PC端组件】
	+修复`radios`组件`titleName`偶然无法传值的bug
	*新增tag标签样式类
	*layout组件的title属性改为选填，不填写自动为菜单路径；
	*pc端网页的标题无需在pages.json中配置，根据菜单自动生成。
	*客户端sdk增加日期操作函数；
	*优化用户、角色、菜单等基础业务模块代码，admin/menu/getParentList 改名为： admin/menu/listByType ，如更新基础业务模块，请重新配置admin/menu/getParentList权限的路径名称。

## 1.1.4.20200815
* 【base-cloud 云函数公共模块】
	增强分页查询方法`this.paginate()` ，支持动态区间查询条件。
	增加分页数据组装接口`this.getPage()`
	
## 1.1.3.20200815
* 【base-cloud-client.js 客户端sdk】
  + `this.bcc.call()`增加调试模式配置项，开启调试模式后，将在控制台输出请求的路径和参数（仅开发模式下有效，编译发布后不会输出），提升开发调试效率。
  + `this.bcc.call()`增加表单提交参数值类型自动转换配置项，增强自动转换参数值类型
  + `this.bcc.submit()`增加表单自动校验提交时，自动转换参数值类型配置项

* 【PC端组件】
  + 修复 `paginate` 组件显示省略号太多的bug
  + `conditions` 组件增加外显表单数量属性，可以控制外显的筛选表单数量。修复日期重置时，结束日期未重置的bug。

